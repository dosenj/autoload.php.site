<?php

// require __DIR__ . '/Foo.php';   works well
// require __DIR__ . '/Bar.php';   works well

// require 'Foo.php';    works well
// require 'Bar.php';    works well

// require 'include/A.php';    works well

/////////////////////////////////////////////////////////////////////////
// works well

/* spl_autoload_register(function($class){
	require $class . '.php';
}); */

/////////////////////////////////////////////////////////////////////////
// works well

/* spl_autoload_register(function($class_name){

	$file_name = $class_name . '.php';

	if(file_exists($file_name)){
		require $file_name;
	}

}); */

/////////////////////////////////////////////////////////////////////////

function incFiles($class_name)
{
	$file_name = $class_name . '.php';

	if(file_exists($file_name)){
		require $file_name;
	}
}

spl_autoload_register('incFiles'); 

function addFiles($class_name)
{
	$file_name = 'include/' . $class_name . '.php';

	if(file_exists($file_name)){
		require $file_name;
	}
}

spl_autoload_register('addFiles');

function moreFiles($class_name)
{
	$file_name = 'include/app/' . $class_name . '.php';

	if(file_exists($file_name)){
		require $file_name;
	}
}

spl_autoload_register('moreFiles');

/////////////////////////////////////////////////////////////////////////

// __autoload function works well

/* function __autoload($class_name)
{	
	$file_name = $class_name . '.php';

	if(file_exists($file_name)){
		require $file_name;
	}
} */

/////////////////////////////////////////////////////////////////////////

$foo = new Foo();
$bar = new Bar();

$a = new A();

$b = new B();

var_dump($foo);
var_dump($bar);

var_dump($a);

var_dump($b);